/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxweek2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author BankMMT
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckVerticalPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        int row = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'}, {'-', '-', 'O'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        int row = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
}
