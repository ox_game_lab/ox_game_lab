/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxweek2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author BankMMT
 */
public class TDDTest {
    
    public TDDTest() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckVerticalPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        int row = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'}, {'-', '-', 'O'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        int row = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckHorizontalPlayerORow2Win() {
        char table[][] = {{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 2;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckHorizontalPlayerORow3Win() {
        char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 3;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckVerticalPlayerXCol1Win() {
        char table[][] = {{'X', '-', '-'}, {'X', '-', '-'}, {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        int row = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        int row = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol3Win() {
        char table[][] = {{'-', '-', 'X'}, {'-', '-', 'X'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        int row = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckHorizontalPlayerXRow1Win() {
        char table[][] = {{'X', 'X', 'X'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckHorizontalPlayerXRow2Win() {
        char table[][] = {{'-', '-', '-'}, {'X', 'X', 'X'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        int row = 2;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckHorizontalPlayerXRow3Win() {
        char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int col = 1;
        int row = 3;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckXPlayerXX1Win(){
        char table[][] = {{'X', '-', '-'}, {'-', 'X', '-'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }
    @Test
    public void testCheckXPlayerXX2Win(){
        char table[][] = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }
    @Test
    public void testCheckXPlayerOX1Win(){
        char table[][] = {{'O', '-', '-'}, {'-', 'O', '-'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }
    @Test
    public void testCheckXPlayerOX2Win(){
        char table[][] = {{'-', '-', 'O'}, {'-', 'O', '-'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }
    @Test 
    public void testCheckDraw(){
        int count = 8;
        assertEquals(true, OXProgram.checkDraw(count));
    }

}
